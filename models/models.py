from peewee import SqliteDatabase, Model, CharField, ForeignKeyField, \
    IntegerField, FloatField

test_db = SqliteDatabase('test.db', pragmas={'journal_mode': 'wal'})


class BaseModel(Model):
    class Meta:
        database = test_db


class User(BaseModel):
    username = CharField(max_length=20, unique=True)
    first_name = CharField(max_length=15)
    last_name = CharField(max_length=25)
    password = CharField(max_length=15)


class Teacher(User):
    code = CharField(max_length=20, unique=True)


class Course(BaseModel):
    name = CharField(max_length=15)
    teacher = ForeignKeyField(Teacher, backref='courses')


class Student(User):
    code = CharField(max_length=20, unique=True)


class Grade(BaseModel):
    student = ForeignKeyField(Student, backref='grades_for_student')
    grade = FloatField()
    course = ForeignKeyField(Course, backref='grades_for_course')
