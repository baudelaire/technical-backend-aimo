import jwt


def create_jwt(data):
    return jwt.encode(data, 'secret',
                      algorithm='HS256')


def decode_jwt(j_w_t):
    return jwt.decode(j_w_t, 'secret', algorithms=['HS256'])
