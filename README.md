# Documentación de api

## 1. Registrar profesores
- api: http://localhost:8000/api/register-teacher/
- Método: Post
- Ejemplo :
{
  "username": "baudelaire",
  "password": "1234",
  "first_name": "erik",
  "last_name": "Guido"
}
- respuesta:{"access_token": "teacher_token"}
* Devuelve un jwt
## 2. Login
- api: http://localhost:8000/api/login
- Método: Post
- Ejemplo :
{
  "username": "baudelaire",
  "password": "1234",
}
- respuesta:{"access_token": "teacher_token"}
* Devuelve un jwt

## 3. Registrar Cursos
- Api: http://localhost:8000/api/register-course/
- Header Authorization: Token teacher_token
- Método: Post
- Ejemplo :
{"name":"lenguaje","teacher":1}
en teacher se le pasa el id del profesor
- Respuesta:
{
    "name": "Lenguaje",
    "teacher": 1
}
## 4. Registrar Alumno
- Api: http://localhost:8000/api/register-student/
- Header Authorization: Token teacher_token
- Método: Post
- Ejemplo :
{"username":"student","password":"1234","first_name":"student_name
","last_name":"student_last"} 
- Respuesta:
{
    "username": "stusent",
    "first_name": "student_name",
    "password": "baudelaire123",
    "last_name": "student_last"
}
## 5. Registrar Nota
- Api: http://localhost:8000/api/register-grade/
- Header Authorization: Token teacher_token
- Método: Post
- Ejemplo :
{"student":1,"course":1,"grade":20.5}
- Respuesta:
{
    "status_code": "400",
    "message": "Grade must not be greater than 20."
}
- Ejemplo 2 :
{
    "grade": 15.5,
    "course": 1,
    "student": 1
} En  course y student se les pasa los ids respectivos
- Respuesta 2:
{
    "grade": 15.5,
    "course": 1,
    "student": 1
}
## 6. Listar Mis Notas
- Api: http://localhost:8000/api/my-grades/
* __Sera necesario loguearse como estudiante para poder visualizar solo sus
 propias notas.__
- Header Authorization: Token student_token
- Método: GET

- Respuesta:
{"grades": [[{"grade": 12.5, "course": {"name": "Lenguaje"}}, {"grade": 13.5, "course": {"name": "Lenguaje"}}, {"grade": 13.5, "course": {"name": "Matematicas"}}, {"grade": 18.5, "course": {"name": "Matematicas"}}, {"grade": 15.5, "course": {"name": "Matematicas"}}], {}]}
