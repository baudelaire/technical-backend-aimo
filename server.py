# Run with "python server.py"
from bottle import run, route, request, hook, response
from models.models import *
from serializers.schemas import course_schema, teacher_schema, login_schema, \
    student_schema, grade_schema, grades_schema
from utils.verify_authentication import requires_auth

_allow_origin = '*'
_allow_methods = 'PUT, GET, POST, DELETE, OPTIONS'
_allow_headers = 'Authorization, Origin, Accept, Content-Type, X-Requested-With'


@hook('after_request')
def enable_cors():
    '''Add headers to enable CORS'''

    response.headers['Access-Control-Allow-Origin'] = _allow_origin
    response.headers['Access-Control-Allow-Methods'] = _allow_methods
    response.headers['Access-Control-Allow-Headers'] = _allow_headers


if __name__ == '__main__':
    test_db.connect()
    User.create_table(True)
    Teacher.create_table(True)
    Course.create_table(True)
    Student.create_table(True)
    Grade.create_table(True)


@route("/api/register-course/", method="POST")
@requires_auth
def register_course(user_type, username):
    if user_type != "teacher":
        return {"message": "Students don't have access to register course"}
    course = course_schema.process_input(request.json)
    return course


@route("/api/register-teacher/", method="POST")
def register_teacher():
    teacher = teacher_schema.process_input(request.json)
    return teacher


@route('/', method='POST')
@route("/api/login", method='POST')
def login():
    login = login_schema.check_auth(request.json)
    return login


@route("/api/register-grade/", method="POST")
@requires_auth
def register_grade(user_type, username):
    if user_type != "teacher":
        return {"message": "Students don't have access to register grades"}
    grade = grade_schema.process_input(request.json)
    return grade


@route("/api/register-student/", method="POST")
@requires_auth
def register_student(user_type, username):
    if user_type != "teacher":
        return {"message": "Students don't have access to register students"}
    student = student_schema.process_input(request.json)
    return student


@route("/api/my-grades/", method="GET")
@requires_auth
def list_my_grades(user_type, username):
    if user_type != "student":
        return {"message": "Only Students can see theirs grades"}
    grades = Student.get(username=username).grades_for_student
    grades = grades_schema.dump(grades)
    return {"grades": grades}


run(host='localhost', reloader=True, port=8000, debug=True)
