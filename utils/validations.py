from marshmallow import ValidationError


def validate_grade(n):
    if n < 0:
        return {"message": "Grade must be greater than 0.",
                "status_code": "400"}
    if n > 20:
        return {"message": "Grade must not be greater than 20.",
                "status_code": "400"}
    return {"message": "ok.", "status_code": "200"}
