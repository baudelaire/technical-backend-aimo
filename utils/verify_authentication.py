from utils.decode import decode_jwt
from functools import wraps
from bottle import request


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.headers.get("Authorization")
        if not auth:
            resp = ({"message": "Please authenticate.", "status_code": "401"})
            return resp
        data_decode = decode_jwt(request.headers.get("Authorization")[6:])
        if not all(key in data_decode for key in ("username", "user_type")):
            resp = ({"message": "Invalid Token", "status_code": "401"})
            return resp
        kwargs["user_type"] = data_decode["user_type"]
        kwargs["username"] = data_decode["username"]
        return f(*args, **kwargs)

    return decorated
