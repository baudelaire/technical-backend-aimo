# Run with "python client.py"
from bottle import get, run, static_file, error, route


@route('/statics/<filename:re:.*\.js>')
def send_static(filename):
    return static_file(filename, root='./statics/')


@get('/')
def index():
    return static_file('templates/index.html', root=".")


@error(404)
def error404(error):
    return 'Nothing here, sorry'


run(host='localhost', port=5000)
