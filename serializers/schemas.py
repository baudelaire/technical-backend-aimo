from marshmallow import (Schema,
                         fields,
                         validate,
                         pre_load)
from utils.validations import validate_grade
from utils.decode import create_jwt
from models.models import *
from bottle import request


class CourseSchema(Schema):
    id = fields.Int(dump_only=True)
    name = fields.Str(
        required=True,
        validate=[validate.Length(min=6, max=15)]
    )
    teacher = fields.Int(required=True)

    @pre_load
    def process_input(self, data, **kwargs):
        data["name"] = data["name"].capitalize()
        if not Teacher.select().where(Teacher.id == data["teacher"]).exists():
            return {"message": "Teacher not exists",
                    "status": 404}
        teacher = Teacher.get(id=data["teacher"])
        Course.create(name=data["name"], teacher=teacher)
        return data


class TeacherSchema(Schema):
    id = fields.Int(dump_only=True)
    username = fields.Str(
        required=True,
        validate=[validate.Length(min=6, max=20)]
    )
    password = fields.Str(
        required=True, validate=[validate.Length(min=8, max=15)],
        load_only=True
    )
    first_name = fields.Str(required=True, validate=[validate.Length(min=1,
                                                                     max=15)])
    last_name = fields.Str(required=True, validate=[validate.Length(min=1,
                                                                    max=25)])

    @pre_load
    def process_input(self, data, **kwargs):
        data["username"] = data["username"].lower().strip()
        if Teacher.select().where(Teacher.username == data[
            "username"]).exists():
            return {"message": "Username already exists in our database",
                    "status": 400}
        else:
            teacher = Teacher(username=request.json['username'],
                              password=request.json["password"],
                              first_name=request.json["first_name"],
                              last_name=request.json["last_name"],
                              code=request.json["username"]
                              )
            teacher.save()
            j_w_t = create_jwt(
                data={'username': teacher.username, "user_type": "teacher"})
        return {"access_token": j_w_t}


class StudentSchema(Schema):
    id = fields.Int(dump_only=True)
    username = fields.Str(
        required=True,
        validate=[validate.Length(min=6, max=20)]
    )
    password = fields.Str(
        required=True, validate=[validate.Length(min=8, max=15)],
        load_only=True
    )
    first_name = fields.Str(required=True, validate=[validate.Length(min=1,
                                                                     max=15)])
    last_name = fields.Str(required=True, validate=[validate.Length(min=1,
                                                                    max=25)])

    @pre_load
    def process_input(self, data, **kwargs):
        data["username"] = data["username"].lower().strip()
        if Student.select().where(Student.username == data[
            "username"]).exists():
            return {"message": "Username already exists in our database",
                    "status": 400}
        else:
            student = Student(username=request.json['username'],
                              password=request.json["password"],
                              first_name=request.json["first_name"],
                              last_name=request.json["last_name"],
                              code=request.json["username"]
                              )
            student.save()

        return data


class LoginSchema(Schema):
    username = fields.Str(
        required=True,
        validate=[validate.Length(min=6, max=20)]
    )
    password = fields.Str(
        required=True, validate=[validate.Length(min=8, max=15)],
        load_only=True
    )

    def check_auth(self, data, **kwargs):
        data["username"] = data["username"].lower().strip()
        if not Teacher.select().where(Teacher.username == data[
            "username"]).exists() and not Student.select().where(
            Student.username == data["username"]).exists():
            return {"message": "Username is not in our database",
                    "status": 400}
        elif not Teacher.select().where(
                Teacher.username == data["username"],
                Teacher.password == data["password"]).exists() and not \
                Student.select().where(
                    Student.username == data["username"],
                    Student.password == data["password"]).exists():
            return {"message": "Password incorrect not in our database",
                    "status": 400}
        elif Teacher.select().where(
                Teacher.username == data["username"],
                Teacher.password == data["password"]).exists():
            teacher = Teacher.get(username=data["username"],
                                  password=data["password"])
            j_w_t = create_jwt(
                data={"username": teacher.username, "user_type": "teacher"})
            return {"access_token": j_w_t, "user_type": "teacher"}

        elif Student.select().where(
                Student.username == data["username"],
                Student.password == data["password"]).exists():
            student = Student.get(username=data["username"],
                                  password=data["password"])
            j_w_t = create_jwt(
                data={"username": student.username, "user_type": "student"})
            return {"access_token": j_w_t, "user_type": "student"}


class GradeSchema(Schema):
    id = fields.Int(dump_only=True)
    student = fields.Int(required=True)
    course = fields.Int(required=True)
    grade = fields.Float(required=True)

    @pre_load
    def process_input(self, data, **kwargs):
        if not Student.select().where(Student.id == data["student"]).exists():
            return {"message": "Student not exists",
                    "status": 404}
        if not Course.select().where(Course.id == data["course"]).exists():
            return {"message": "Course not exists",
                    "status": 404}
        course = Course.get(id=data["course"])
        student = Student.get(id=data["student"])
        validate = validate_grade(data["grade"])
        if validate["status_code"] != "200":
            return validate
        Grade.create(course=course, grade=data["grade"], student=student)
        return data


class CourseForSchema(Schema):
    name = fields.Str(dump_only=True)


class GradesSchema(Schema):
    course = fields.Nested(CourseForSchema)
    grade = fields.Float(dump_only=True)


course_schema = CourseSchema()
teacher_schema = TeacherSchema()
login_schema = LoginSchema()
student_schema = StudentSchema()
grade_schema = GradeSchema()
grades_schema = GradesSchema(many=True)
